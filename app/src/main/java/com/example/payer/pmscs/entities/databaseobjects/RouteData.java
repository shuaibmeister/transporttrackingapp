package com.example.payer.pmscs.entities.databaseobjects;

import android.support.annotation.NonNull;

import com.example.payer.pmscs.entities.StringConstraints;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shuaib on 10/18/16.
 */

public class RouteData extends AbstractConvertibleImpl {

    long id = -1;
    String name = "";
    String from = "";
    String to = "";
    String description = "";

    public RouteData(){
    }

    public RouteData(JSONObject jsonObject) {
        fromJson(jsonObject);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    @NonNull
    public JSONObject toJson() {
        try {
            return new JSONObject().put(StringConstraints.Route.ID, id)
                    .put(StringConstraints.Route.NAME, name)
                    .put(StringConstraints.Route.FROM, from)
                    .put(StringConstraints.Route.TO, to)
                    .put(StringConstraints.Route.DESCRIPTION, description);
        } catch (JSONException e){
            e.printStackTrace();
            return new JSONObject();
        }
    }

    @Override
    public Map toMap() {
        Map routeMap = new HashMap<>();
        routeMap.put(StringConstraints.TrackingFields.ID, id);
        routeMap.put(StringConstraints.Route.NAME, name);
        routeMap.put(StringConstraints.Route.FROM, from);
        routeMap.put(StringConstraints.Route.TO, to);
        routeMap.put(StringConstraints.Route.DESCRIPTION, description);
        return routeMap;
    }

    @Override
    public Object fromJson(JSONObject jsonObject) {
        try {
            setId(jsonObject.getLong(StringConstraints.Route.ID));
            setName(jsonObject.getString(StringConstraints.Route.NAME));
            setFrom(jsonObject.getString(StringConstraints.Route.FROM));
            setTo(jsonObject.getString(StringConstraints.Route.TO));
            setDescription(jsonObject.getString(StringConstraints.Route.DESCRIPTION));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return this;
    }
}
