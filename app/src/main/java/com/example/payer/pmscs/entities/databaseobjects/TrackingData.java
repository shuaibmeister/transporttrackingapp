package com.example.payer.pmscs.entities.databaseobjects;

import com.example.payer.pmscs.entities.StringConstraints;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shuaib on 10/19/16.
 */

public class TrackingData extends AbstractConvertibleImpl {

    long id = -1;
    double lat;
    double lon;
    float accuracy;
    long userId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Override
    public JSONObject toJson() {
        try {
            return new JSONObject().put(StringConstraints.TrackingFields.ID, id)
                    .put(StringConstraints.TrackingFields.KEY_LAT, lat)
                    .put(StringConstraints.TrackingFields.KEY_LON, lon)
                    .put(StringConstraints.TrackingFields.KEY_ACCURACY, accuracy)
                    .put(StringConstraints.TrackingFields.KEY_USER_ID, userId);
        } catch (JSONException e) {
            e.printStackTrace();
            return new JSONObject();
        }
    }

    @Override
    public Map toMap() {
        Map trackingMap = new HashMap<>();
        trackingMap.put(StringConstraints.TrackingFields.ID, String.valueOf(id));
        trackingMap.put(StringConstraints.TrackingFields.KEY_LAT, String.valueOf(lat));
        trackingMap.put(StringConstraints.TrackingFields.KEY_LON, String.valueOf(lon));
        trackingMap.put(StringConstraints.TrackingFields.KEY_ACCURACY, String.valueOf(accuracy));
        trackingMap.put(StringConstraints.TrackingFields.KEY_USER_ID, String.valueOf(userId));
        return trackingMap;
    }

    @Override
    public Object fromJson(JSONObject jsonObject) {
        try {
            setId(jsonObject.getLong(StringConstraints.TrackingFields.ID));
            setLat(jsonObject.getLong(StringConstraints.TrackingFields.KEY_LAT));
            setLon(jsonObject.getLong(StringConstraints.TrackingFields.KEY_LON));
            setAccuracy(jsonObject.getLong(StringConstraints.TrackingFields.KEY_ACCURACY));
            setUserId(jsonObject.getLong(StringConstraints.TrackingFields.KEY_USER_ID));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return this;
    }
}
