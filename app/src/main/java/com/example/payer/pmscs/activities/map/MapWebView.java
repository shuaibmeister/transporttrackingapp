package com.example.payer.pmscs.activities.map;

import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.example.payer.pmscs.R;
import com.example.payer.pmscs.api.WebUrls;
import com.example.payer.pmscs.commontask.CommonTask;
import com.example.payer.pmscs.entities.GlobalStaticObjects;
import com.example.payer.pmscs.entities.StringConstraints;
import com.example.payer.pmscs.utils.js.AssetReader;
import com.example.payer.pmscs.utils.js.JavaToJs;
import com.example.payer.pmscs.utils.js.JsReader;
import com.example.payer.pmscs.utils.location.PositionManager;
import com.example.payer.pmscs.utils.location.WebClient;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MapWebView.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MapWebView#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapWebView extends Fragment {

    private OnFragmentInteractionListener mListener;
    private PositionManager locationManager;
//    private WebView webView;

    public MapWebView() {
        // Required empty public constructor
    }

    public static MapWebView newInstance() {
        MapWebView fragment = new MapWebView();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_map_web_view, container, false);
        initWebView(v);
        return v;
    }

    private void initWebView(View v) {
        if(CommonTask.isConnectedToInternet(getActivity())) {
            WebView webView = (WebView) v.findViewById(R.id.map_webview);
            GlobalStaticObjects.routeData = GlobalStaticObjects.routeDatas.get(getRouteId());
            webView.loadUrl(WebUrls.getBusPositionApi(GlobalStaticObjects.routeData.getId()));

            WebSettings webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            AssetReader assetReader = new JsReader(getActivity());
            webView.setWebViewClient(new WebClient(assetReader));
        }
//        javaToJsTest(assetReader);
    }

    private void javaToJsTest(AssetReader assetReader){
        HashMap<String, Object[]> arguments = new HashMap<>();
        arguments.put("sayHello", new Object[]{"say hello"});
        JavaToJs javaToJs = new JavaToJs(assetReader.readFile("loadMyLocation.js"), arguments);
        javaToJs.callFunction("sayHello");
    }

    private long getRouteId() {
        long i = 1;
        Bundle bundle = this.getArguments();
        if (bundle != null) {
//            i = bundle.getInt(StringConstraints.Route.TITLE + "_" + StringConstraints.Route.ID);
            i = bundle.getLong(StringConstraints.Route.TITLE);
//            Toast.makeText(getActivity(), bundle.getString(StringConstraints.Route.TITLE + "_" + StringConstraints.Route.NAME), Toast.LENGTH_SHORT).show();
        }
        return i;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
