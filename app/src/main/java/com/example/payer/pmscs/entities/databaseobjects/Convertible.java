package com.example.payer.pmscs.entities.databaseobjects;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by shuaib on 10/18/16.
 */

public interface Convertible {
    JSONObject toJson();
    JSONObject toJson(String jsonString);
    Map toMap();
    Object fromJson(JSONObject jsonObject);
//    default String toString(){
//        return toJson().toString();
//    }
    String toString();
}
