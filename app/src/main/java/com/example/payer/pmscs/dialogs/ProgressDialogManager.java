package com.example.payer.pmscs.dialogs;

/**
 * Created by shuaib on 8/23/16.
 */

public interface ProgressDialogManager {
    void setProgressDialog(Object dialog);
    void showProgress();
    void hideProgress();
    void hideProgressWithDelay(int delay);
    void setProgressValue(int value);
}
