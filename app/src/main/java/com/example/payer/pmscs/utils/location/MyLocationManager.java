package com.example.payer.pmscs.utils.location;

import android.content.Context;
import android.location.LocationManager;

import com.example.payer.pmscs.entities.Observer;
import com.example.payer.pmscs.entities.databaseobjects.TrackingData;
import com.example.payer.pmscs.utils.CustomLog;

/**
 * Created by shuaibrahman on 10/17/16.
 */

public class MyLocationManager implements PositionManager, Observer {

    private LocationManager locationManager;
    private MyLocationListener locationListener;
    private Observer notifyObserver;

    private long minTime = 60 * 3 * 1000;
    private float minDistance = 50;

    private TrackingData lastTrackingData;

    public MyLocationManager(Context context, Observer observer) {
        this.locationListener = new MyLocationListener();
        locationListener.setObserver(this);
        this.notifyObserver = observer;
        this.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    @Override
    public TrackingData getLocation() {
        return lastTrackingData;
    }

    @Override
    public long getMinTime() {
        return minTime;
    }

    @Override
    public void setMinTime(long minTime) {
        this.minTime = minTime;
    }

    @Override
    public float getMinDistance() {
        return minDistance;
    }

    @Override
    public void setMinDistance(float minDistance) {
        this.minDistance = minDistance;
    }

    @Override
    public Observer getNotifyObserver() {
        return notifyObserver;
    }

    @Override
    public void setNotifyObserver(Observer notifyObserver) {
        this.notifyObserver = notifyObserver;
    }

    @Override
    public void startTracking() throws SecurityException{
        CustomLog.print("starting tracking");
        locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, minTime, minDistance, locationListener);
        locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, minTime, minDistance, locationListener);
    }

    @Override
    public void stopTracking() throws SecurityException {
        CustomLog.print("stop tracking");
        locationManager.removeUpdates(locationListener);
    }

    @Override
    public Object onUpdate(Object... objects) {
        this.lastTrackingData = (TrackingData) objects[0];
        notifyObserver.onUpdate(lastTrackingData);
        return lastTrackingData;
    }
}
