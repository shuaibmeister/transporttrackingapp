package com.example.payer.pmscs.entities;

import com.example.payer.pmscs.utils.CustomLog;
import com.example.payer.pmscs.utils.exceptions.UndefinedObjectException;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by shuaib on 10/16/16.
 */

public class ApiDataObjectImpl implements ApiDataObject {

    private Object obj = null;
    private Map header = null;
    private String body = null;

    public ApiDataObjectImpl(Object object) {
        this.obj = object;
        validateObject();
    }

    @Override
    public Object getObject() {
        return obj;
    }

    @Override
    public Map getHeader() {
        return header;
    }

    @Override
    public String getBody() {
        return body;
    }

    @Override
    public void setHeader(Map header) {
        this.header = header;
    }

    @Override
    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String getObjectType() {
        if (obj instanceof Map) {
            return mapType;
        } else if (obj instanceof JSONObject) {
            return jsonType;
        } else if (obj == null || obj.equals(JSONObject.NULL)) {
            return nullType;
        }
        return undefinedType;
    }

    @Override
    public void addKeyValuePair(String key, String value) {
        String objectType = getObjectType();

        if (objectType.equals(jsonType)) {
            try {
                ((JSONObject) obj).put(key, value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (objectType.equals(mapType)) {
            ((Map) obj).put(key, value);
        }
    }

    @Override
    public void removeKeyValuePair(String key) {

    }

    @Override
    public void validateObject() {
        try {
            if(isNull()){
//                throw new NullPointerException();
                CustomLog.print("object is null");
            } else if (!isDefinedObject()) {
                throw new UndefinedObjectException();
            }
        } catch (UndefinedObjectException | NullPointerException e){
            e.printStackTrace();
        }
    }

    private boolean isDefinedObject(){
        if(getObjectType().equals(undefinedType)){
            return false;
        }
        return true;
    }

    private boolean isNull(){
        if(getObjectType().equals(nullType)){
            return true;
        }
        return false;
    }
}
