package com.example.payer.pmscs.volley.ApiResponseActions;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.payer.pmscs.activities.map.MapViewActivity;
import com.example.payer.pmscs.api.JsonStringConstraints;
import com.example.payer.pmscs.commontask.CommonTask;
import com.example.payer.pmscs.commontask.CommonValues;
import com.example.payer.pmscs.entities.GlobalStaticObjects;
import com.example.payer.pmscs.entities.databaseobjects.UserData;
import com.example.payer.pmscs.utils.CustomToast;
import com.example.payer.pmscs.volley.VolleyResponseActions;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by payer on 10/28/16.
 */

public class LoginResponse implements VolleyResponseActions {

    private Context context;

    public LoginResponse(Context context){
        this.context = context;
    }

    @Override
    public void onApiCallSuccess(int reqId, JSONObject responseJson) throws JSONException {
        GlobalStaticObjects.userData = new UserData(responseJson.getJSONArray(JsonStringConstraints
                .ApiResponseFields.KEY_DATA).getJSONObject(0));

        CommonTask.saveDataInPreference(context, CommonValues.SharedPrefKeyValuePairs.KEY_LOGGED_IN,
                CommonValues.SharedPrefKeyValuePairs.VALUE_YES);

        Intent successfulLoginIntent = new Intent(context, MapViewActivity.class);
        context.startActivity(successfulLoginIntent);
    }

    @Override
    public void onApiCallFailure(int reqId, JSONObject responseJson) throws JSONException {
        CustomToast.makeToast(context, responseJson.getString(
                JsonStringConstraints.ApiResponseFields.KEY_MESSAGE), Toast.LENGTH_LONG);
    }

    @Override
    public void onApiCallError(int reqId, VolleyError error) {
        CustomToast.makeToast(context, "Could not connect to server", Toast.LENGTH_LONG);
    }
}
