package com.example.payer.pmscs.activities.map;

import android.app.ActionBar;
import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.example.payer.pmscs.R;
import com.example.payer.pmscs.activities.BaseActivity;
import com.example.payer.pmscs.api.JsonStringConstraints;
import com.example.payer.pmscs.api.WebUrls;
import com.example.payer.pmscs.commontask.CommonTask;
import com.example.payer.pmscs.entities.ApiDataObjectImpl;
import com.example.payer.pmscs.entities.GlobalStaticObjects;
import com.example.payer.pmscs.entities.databaseobjects.RouteData;
import com.example.payer.pmscs.entities.StringConstraints;
import com.example.payer.pmscs.volley.SendVolleyApiRequest;
import com.example.payer.pmscs.volley.VolleyApiRequest;
import com.example.payer.pmscs.volley.VolleyResponseActions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MapViewActivity extends BaseActivity implements MapWebView.OnFragmentInteractionListener {

    //    private PositionManager mPositionManager;
//    private List<RouteData> routeDatas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mPositionManager = new MyLocationManager(this);
//        mPositionManager.startTracking();
        addAppTitle();
        fetchAllRoutes();
    }

    private void addAppTitle() {
        TextView titleTextView = new TextView(this);
        titleTextView.setText("Welcome to Crowd Source Based Vehicle Tracking");
        titleTextView.setLayoutParams(CommonTask.getLayoutParam(0, 0, 0, 200));
        titleTextView.setGravity(Gravity.CENTER);
        titleTextView.setTextSize(20);
        fragmentLayout.addView(titleTextView);
    }

    private void fetchAllRoutes() {
        VolleyResponseActions responseActions = getVolleyResponseActions();
        VolleyApiRequest apiRequest = new SendVolleyApiRequest(MapViewActivity.this, WebUrls.ROUTE_FETCH_API,
                new ApiDataObjectImpl(null), Request.Method.POST, responseActions, 1);
        apiRequest.sendRequest();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onStart() {
        super.onStart();
//        mPositionManager.startTracking();
    }

    @Override
    protected void onPause() {
        super.onStart();
//        mPositionManager.stopTracking();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        mPositionManager.stopTracking();
    }

    private VolleyResponseActions getVolleyResponseActions() {
        return new VolleyResponseActions() {
            @Override
            public void onApiCallSuccess(int reqId, JSONObject responseJson) throws JSONException {
                JSONArray routes = responseJson.getJSONArray(JsonStringConstraints.ApiResponseFields.KEY_DATA);
                for (int i = 0; i < routes.length(); i++) {
                    RouteData routeData = new RouteData(routes.getJSONObject(i));
                    GlobalStaticObjects.routeDatas.put(routeData.getId(), routeData);
                    addRouteButton(routeData);
                }
            }

            @Override
            public void onApiCallFailure(int reqId, JSONObject responseJson) throws JSONException {

            }

            @Override
            public void onApiCallError(int reqId, VolleyError error) {

            }
        };
    }

    private void addRouteButton(final RouteData routeData) {
        Button routeButton = new Button(this);
        routeButton.setText(routeData.getDescription());
        routeButton.setLayoutParams(CommonTask.getLayoutParam(10, 0, 10, 10));
        routeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadWebView(routeData.getId());
            }
        });
        routeButton.setBackgroundColor(getResources().getColor(R.color.login_button));
        fragmentLayout.addView(routeButton);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (GlobalStaticObjects.routeDatas.size() > 0) {
            menu.clear();
            for (RouteData routeData : GlobalStaticObjects.routeDatas.values()) {
                menu.add((int) routeData.getId(), (int) routeData.getId(), (int) routeData.getId(), routeData.getName());
            }
        } else {
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        loadWebView(item.getItemId());
        return super.onOptionsItemSelected(item);
    }

    private void loadWebView(long routeId) {
        Fragment mapWebviewFragment = MapWebView.newInstance();
        Bundle b = new Bundle();
        b.putLong(StringConstraints.Route.TITLE, routeId);
        mapWebviewFragment.setArguments(b);
        fragmentLayout.removeAllViews();
        startFragment(mapWebviewFragment);
    }
}
