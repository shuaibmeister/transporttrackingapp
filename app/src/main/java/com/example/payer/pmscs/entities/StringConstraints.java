package com.example.payer.pmscs.entities;

/**
 * Created by shuaib on 10/18/16.
 */

public class StringConstraints {

    public static class User {
        public static final String TITLE = "user";
        public static final String ID = "id";
        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String USER_ID = "username";
        public static final String PASSWORD = "password";
        public static final String EMAIL = "email";
        public static final String PHONE = "phone";
    }

    public static class Route {
        public static final String TITLE = "route";
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String FROM = "from";
        public static final String TO = "to";
        public static final String DESCRIPTION = "description";
    }

    public static class TrackingFields {
        public static final String TITLE = "trackingData";
        public static final String ID = "id";
        public static final String KEY_LAT = "lat";
        public static final String KEY_LON = "lon";
        public static final String KEY_ACCURACY = "accuracy";
        public static final String KEY_USER_ID = "user_id";
        public static final String KEY_ROUTE_ID = "route_id";
    }
}
