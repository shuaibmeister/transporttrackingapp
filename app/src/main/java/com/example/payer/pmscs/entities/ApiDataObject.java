package com.example.payer.pmscs.entities;

import java.util.Map;

/**
 * Created by shuaib on 10/16/16.
 */

public interface ApiDataObject {
    String jsonType = "jsonType";
    String mapType = "mapType";
    String undefinedType = "undefined";
    String nullType = "null";

    Object getObject();
    Map getHeader();
    String getBody();
    void setHeader(Map header);
    void setBody(String body);
    String getObjectType();
    void addKeyValuePair(String key, String value);
    void removeKeyValuePair(String key);
    void validateObject();
}
