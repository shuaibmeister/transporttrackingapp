package com.example.payer.pmscs.volley;

/**
 * Created by shuaib on 4/10/16.
 */
public interface VolleyApiRequest {
    void setRetryCount(int retryCount);
    void setTimeout(int timeout);
    void sendRequest();
    void sendJsonRequest();
    void sendStringRequest();
    void setProgressDialog(Object dialog);
}

