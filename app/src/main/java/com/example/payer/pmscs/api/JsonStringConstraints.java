package com.example.payer.pmscs.api;

/**
 * Created by shuaib on 4/7/16.
 */
public class JsonStringConstraints {

    public static class ApiResponseFields {
        public static final String KEY_MESSAGE = "message";
        public static final String KEY_STATUS = "success";
        public static final String KEY_DATA = "data";
    }

    public static class LoginApiFields {
        public static final String KEY_USERNAME = "username";
        public static final String KEY_PASSWORD = "password";
    }

    public static class CommonStrings {
        public static final String KEY_API = "api_key";
        public static final String VALUE_API = "pmscs";
    }
}
