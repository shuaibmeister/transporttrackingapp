package com.example.payer.pmscs.utils.location.locationobservers;

import android.content.Context;

import com.android.volley.Request;
import com.example.payer.pmscs.api.WebUrls;
import com.example.payer.pmscs.entities.ApiDataObject;
import com.example.payer.pmscs.entities.ApiDataObjectImpl;
import com.example.payer.pmscs.entities.Observer;
import com.example.payer.pmscs.entities.databaseobjects.TrackingData;
import com.example.payer.pmscs.volley.ApiResponseActions.LocationApiCallResponse;
import com.example.payer.pmscs.volley.SendVolleyApiRequest;
import com.example.payer.pmscs.volley.VolleyApiRequest;

/**
 * Created by shuaib on 10/19/16.
 */

public class CheckInObserver implements Observer {

    private Context context;

    public CheckInObserver(Context context){
        this.context = context;
    }

    @Override
    public Object onUpdate(Object... objects) {
        ApiDataObject trackingInfo = new ApiDataObjectImpl(((TrackingData)objects[0]).toMap());
        VolleyApiRequest apiRequest = new SendVolleyApiRequest(context, WebUrls.TRACKING_API, trackingInfo,
                Request.Method.POST, new LocationApiCallResponse(), 1);
        apiRequest.setProgressDialog(null);
        apiRequest.sendRequest();
        return null;
    }
}
