package com.example.payer.pmscs.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.payer.pmscs.R;
import com.example.payer.pmscs.activities.login.LoginActivity;
import com.example.payer.pmscs.commontask.CommonValues;
import com.example.payer.pmscs.entities.GlobalStaticObjects;
import com.example.payer.pmscs.entities.Observer;
import com.example.payer.pmscs.utils.CustomToast;
import com.example.payer.pmscs.utils.PermissionHandler;
import com.example.payer.pmscs.utils.location.MyLocationManager;
import com.example.payer.pmscs.utils.location.PositionManager;
import com.example.payer.pmscs.utils.location.locationobservers.CheckInObserver;
import com.example.payer.pmscs.utils.location.locationobservers.OnWaitingObserver;

import org.json.JSONException;

public class BaseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    protected LinearLayout fragmentLayout;
    protected PositionManager myPositionManager;
    protected Observer checkInObserver;
    protected Observer onWaitingObserver;
    private PermissionHandler permissionHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        initObservers();
        initToolbar();
//        initFab();
        try {
            initNavigationDrawer();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        fragmentLayout = (LinearLayout) findViewById(R.id.fragment_container);
        this.permissionHandler = new PermissionHandler(this);
    }

    private void initObservers() {
        checkInObserver = new CheckInObserver(this);
        onWaitingObserver = new OnWaitingObserver(this);
    }


    protected void startFragment(Fragment fragment) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(fragmentLayout.getId(), fragment);
        fragmentTransaction.commit();
    }

    protected void initNavigationDrawer() throws JSONException {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        setUserName(navigationView);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void setUserName(NavigationView navigationView) throws JSONException {
//        if (getIntent().hasExtra(StringConstraints.User.TITLE)) {
//            Bundle bundle = getIntent().getExtras();
//            UserData userData = new UserData(new JSONObject(bundle.getString(StringConstraints.User.TITLE)));
            View header = navigationView.getHeaderView(0);
            TextView subtitle = (TextView) header.findViewById(R.id.nav_subtitle);
            subtitle.setText(GlobalStaticObjects.userData.getFirstName()+" "+GlobalStaticObjects.userData.getLastName());
//        }
    }

    private void initFab() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.base, menu);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        boolean isLocationPermitted = permissionHandler.isLocationPermissionGranted();

        if (id == R.id.waiting) {
            if(isLocationPermitted) {
                startTracking(onWaitingObserver, "Please wait while the bus arrives");
            }
        } else if (id == R.id.check_in) {
            if(isLocationPermitted) {
                startTracking(checkInObserver, "Tracking has started");
            }
        } else if (id == R.id.sign_out) {
            if (myPositionManager != null) {
                myPositionManager.stopTracking();
            }

            startActivity(new Intent(BaseActivity.this, LoginActivity.class));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void startTracking(Observer observer, String message) {
        initPositionManager(observer);
        try {
            myPositionManager.startTracking();
            CustomToast.makeToast(this, message, Toast.LENGTH_LONG);
        } catch (SecurityException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CommonValues.LOCATION_PERMISSION_ID: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startTracking(checkInObserver, "Tracking has started");
                } else {

                }
                return;
            }
        }
    }

    private void initPositionManager(Observer observer) {
        if (myPositionManager == null) {
            myPositionManager = new MyLocationManager(this, observer);
        } else {
            myPositionManager.setNotifyObserver(observer);
        }
    }
}
