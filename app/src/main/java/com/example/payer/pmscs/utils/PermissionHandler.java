package com.example.payer.pmscs.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.example.payer.pmscs.commontask.CommonValues;

/**
 * Created by shuaibrahman on 10/17/16.
 */

public class PermissionHandler {

    private Context context;

    public PermissionHandler(Context context){
        this.context = context;
    }

    public boolean isLocationPermissionGranted() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    CommonValues.LOCATION_PERMISSION_ID);
            return false;
        } else {
            return true;
        }
    }
}
