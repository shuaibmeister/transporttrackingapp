package com.example.payer.pmscs.entities;

/**
 * Created by shuaib on 10/19/16.
 */

public interface Observer {
    Object onUpdate(Object... objects);
}
