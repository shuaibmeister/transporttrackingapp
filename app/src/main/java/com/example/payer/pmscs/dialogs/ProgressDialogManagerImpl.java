package com.example.payer.pmscs.dialogs;

import android.app.ProgressDialog;
import android.os.Handler;

/**
 * Created by shuaib on 8/23/16.
 */

public class ProgressDialogManagerImpl implements ProgressDialogManager {

    private ProgressDialog dialog = null;

    @Override
    public void setProgressDialog(Object dialog){
        this.dialog = (ProgressDialog) dialog;
    }

    @Override
    public void showProgress() {
        if(dialog != null) {
            dialog.show();
        }
    }

    @Override
    public void hideProgress() {
        if(dialog != null){
            dialog.dismiss();
        }
    }

    @Override
    public void hideProgressWithDelay(int delay) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideProgress();
            }
        }, delay);
    }

    @Override
    public void setProgressValue(int value){
        if(dialog != null) {
            dialog.setProgress(value);
        }
    }
}
