package com.example.payer.pmscs.entities.databaseobjects;

import android.support.annotation.NonNull;

import com.example.payer.pmscs.entities.StringConstraints;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shuaib on 10/18/16.
 */

public class UserData extends AbstractConvertibleImpl {

    long id = -1;
    String firstName = "";
    String lastName = "";
    String username = "";
    String password = "";
    String email = "";
    String phone = "";

    public UserData(){
    }

    public UserData(JSONObject jsonObject) {
        fromJson(jsonObject);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    @NonNull
    public JSONObject toJson() {
        try {
            return new JSONObject().put(StringConstraints.User.ID, id)
                    .put(StringConstraints.User.FIRST_NAME, firstName)
                    .put(StringConstraints.User.LAST_NAME, lastName)
                    .put(StringConstraints.User.USER_ID, username)
                    .put(StringConstraints.User.PASSWORD, password)
                    .put(StringConstraints.User.EMAIL, email)
                    .put(StringConstraints.User.PHONE, phone);
        } catch (JSONException e){
            e.printStackTrace();
            return new JSONObject();
        }
    }

    @Override
    public Map toMap() {
        Map userDataMap = new HashMap<>();
        userDataMap.put(StringConstraints.TrackingFields.ID, id);
        userDataMap.put(StringConstraints.User.FIRST_NAME, firstName);
        userDataMap.put(StringConstraints.User.LAST_NAME, lastName);
        userDataMap.put(StringConstraints.User.USER_ID, username);
        userDataMap.put(StringConstraints.User.PASSWORD, password);
        userDataMap.put(StringConstraints.User.EMAIL, email);
        userDataMap.put(StringConstraints.User.PHONE, phone);
        return userDataMap;
    }

    @Override
    public Object fromJson(JSONObject jsonObject) {
        try {
            setId(jsonObject.getLong(StringConstraints.User.ID));
            setFirstName(jsonObject.getString(StringConstraints.User.FIRST_NAME));
            setLastName(jsonObject.getString(StringConstraints.User.LAST_NAME));
            setUsername(jsonObject.getString(StringConstraints.User.USER_ID));
            setPassword(jsonObject.optString(StringConstraints.User.PASSWORD));
            setEmail(jsonObject.getString(StringConstraints.User.EMAIL));
            setPhone(jsonObject.getString(StringConstraints.User.PHONE));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return this;
    }
}
