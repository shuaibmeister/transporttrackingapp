package com.example.payer.pmscs.volley;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.payer.pmscs.api.JsonStringConstraints;
import com.example.payer.pmscs.base.ApplicationBase;
import com.example.payer.pmscs.commontask.CommonTask;
import com.example.payer.pmscs.dialogs.CommonDialogs;
import com.example.payer.pmscs.dialogs.ProgressDialogManager;
import com.example.payer.pmscs.dialogs.ProgressDialogManagerImpl;
import com.example.payer.pmscs.entities.ApiDataObject;
import com.example.payer.pmscs.entities.ApiDataObjectImpl;
import com.example.payer.pmscs.utils.CustomLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by shuaib on 4/7/16.
 */
public class SendVolleyApiRequest implements VolleyApiRequest {

    private Context context;
    private String url;
    private ApiDataObject dataObject;
    private int requestMethod;
    private int timeout = (int) TimeUnit.SECONDS.toMillis(20);
    private int retryCount = 0;

    protected int reqId;
    protected VolleyResponseActions volleyListenerAction;
    protected ProgressDialogManager progressDialogManager;

    public SendVolleyApiRequest(Context c, String url, ApiDataObject dataObject, int requestMethod,
                                VolleyResponseActions volleyListenerAction, int reqid) {
        this.context = c;
        this.url = url;
        this.dataObject = dataObject;
        this.requestMethod = requestMethod;
        this.volleyListenerAction = volleyListenerAction;
        this.reqId = reqid;
        this.progressDialogManager = new ProgressDialogManagerImpl();
        progressDialogManager.setProgressDialog(CommonDialogs.getApiCallProgressDialog(context));
    }

    @Override
    public void setProgressDialog(Object dialog) {
        progressDialogManager.setProgressDialog(dialog);
    }

    @Override
    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    @Override
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    @Override
    public void sendRequest(){
        dataObject.validateObject();
        addKeyValuePair(dataObject);

        if(dataObject.getObjectType().equals(dataObject.jsonType)){
            sendJsonRequest();
        } else if(dataObject.getObjectType().equals(dataObject.mapType)){
            sendStringRequest();
        } else if(dataObject.getObjectType().equals(dataObject.nullType)){
            sendRequestDefaultMethod();
        }
    }

    @Override
    public void sendJsonRequest(){
        if (CommonTask.isConnectedToInternet(context)) {
            progressDialogManager.showProgress();
            JsonObjectRequest jsonRequest = new JsonObjectRequest(requestMethod, url, (JSONObject) dataObject.getObject(),
                    getJsonRequestSuccessResponseListener(), getVolleyErrorResponseListener()){

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    if (dataObject.getHeader() == null) {
                        return super.getHeaders();
                    }
                    return dataObject.getHeader();
                }

                @Override
                public byte[] getBody() {
                    if (dataObject.getBody() == null) {
                        return super.getBody();
                    }
                    return dataObject.getBody().getBytes();
                }
            };
            jsonRequest.setRetryPolicy(getRequestPolicy());

            ApplicationBase.getInstance().addToRequestQueue(jsonRequest);
        }
    }

    @Override
    public void sendStringRequest() {
        if (CommonTask.isConnectedToInternet(context)) {
            progressDialogManager.showProgress();
            StringRequest stringRequest = new StringRequest(requestMethod, url, getStringRequestSuccessResponseListener(),
                    getVolleyErrorResponseListener()){

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    if (dataObject.getHeader() == null) {
                        return super.getHeaders();
                    }
                    return dataObject.getHeader();
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    if (dataObject.getBody() == null) {
                        return super.getBody();
                    }
                    return dataObject.getBody().getBytes();
                }

                @Override
                public Map<String, String> getParams(){
                    return (Map)dataObject.getObject();
                }
            };
            stringRequest.setRetryPolicy(getRequestPolicy());
            CustomLog.print(((Map)dataObject.getObject()).toString().replace("]","\n"));
            ApplicationBase.getInstance().addToRequestQueue(stringRequest);
        }
    }

    protected void sendRequestDefaultMethod() {
        ApiDataObject newDataObject = new ApiDataObjectImpl(new HashMap<>());
        newDataObject.setHeader(dataObject.getHeader());
        newDataObject.setBody(dataObject.getBody());
        addKeyValuePair(newDataObject);
        dataObject = newDataObject;
        sendStringRequest();
    }

    protected RetryPolicy getRequestPolicy() {
        return new DefaultRetryPolicy(timeout, retryCount, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    }

    protected Response.Listener<JSONObject> getJsonRequestSuccessResponseListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialogManager.hideProgress();
                CustomLog.print("response " + response.toString());
                callResponseAction(response);
            }
        };
    }

    protected Response.Listener<String> getStringRequestSuccessResponseListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialogManager.hideProgress();
                try {
                    callResponseAction(new JSONObject(response));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                CustomLog.print("response " + response);
            }
        };
    }

    protected Response.ErrorListener getVolleyErrorResponseListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialogManager.hideProgress();
                error.printStackTrace();
//                if(error.networkResponse.data!=null) {
//                    try {
//                        String body = new String(error.networkResponse.data,"UTF-8");
//                        CustomLog.print(body);
//                    } catch (UnsupportedEncodingException e) {
//                        e.printStackTrace();
//                    }
//                }
                volleyListenerAction.onApiCallError(reqId, error);
            }
        };
    }

    protected void callResponseAction(JSONObject response){
        try {
            if (response.getBoolean(JsonStringConstraints.ApiResponseFields.KEY_STATUS)) {
                volleyListenerAction.onApiCallSuccess(reqId, response);
            } else {
                volleyListenerAction.onApiCallFailure(reqId, response);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addKeyValuePair(ApiDataObject dataObject){
        dataObject.addKeyValuePair(JsonStringConstraints.CommonStrings.KEY_API, JsonStringConstraints.CommonStrings.VALUE_API);
    }
}
