package com.example.payer.pmscs.entities.databaseobjects;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by shuaib on 10/19/16.
 */

public abstract class AbstractConvertibleImpl implements Convertible {

    public abstract Map<String, String>toMap();
    @Override
    public JSONObject toJson(String jsonString) {
        try {
            return new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
            return new JSONObject();
        }
    }

    @Override
    public String toString() {
        return toJson().toString();
    }
}
