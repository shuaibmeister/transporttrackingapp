package com.example.payer.pmscs.activities.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.example.payer.pmscs.R;
import com.example.payer.pmscs.activities.map.MapViewActivity;
import com.example.payer.pmscs.activities.registration.RegisterActivity;
import com.example.payer.pmscs.api.JsonStringConstraints;
import com.example.payer.pmscs.api.WebUrls;
import com.example.payer.pmscs.commontask.CommonTask;
import com.example.payer.pmscs.commontask.CommonValues;
import com.example.payer.pmscs.entities.ApiDataObject;
import com.example.payer.pmscs.entities.ApiDataObjectImpl;
import com.example.payer.pmscs.entities.GlobalStaticObjects;
import com.example.payer.pmscs.entities.databaseobjects.UserData;
import com.example.payer.pmscs.utils.CustomToast;
import com.example.payer.pmscs.volley.ApiResponseActions.LoginResponse;
import com.example.payer.pmscs.volley.SendVolleyApiRequest;
import com.example.payer.pmscs.volley.VolleyApiRequest;
import com.example.payer.pmscs.volley.VolleyResponseActions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private String username, password;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText Username = (EditText) findViewById(R.id.username);
        final EditText Password = (EditText) findViewById(R.id.password);
        final Button login = (Button) findViewById(R.id.btnLogin);
        final Button registration = (Button) findViewById(R.id.btnRegistration);


        registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                LoginActivity.this.startActivity(registerIntent);
            }
        });


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = Username.getText().toString();
                password = Password.getText().toString();

                ApiDataObject loginInfoHashmap = new ApiDataObjectImpl(getLoginApiMap());
                VolleyResponseActions responseActions = getVolleyResponseActions();
                VolleyApiRequest apiRequest = new SendVolleyApiRequest(LoginActivity.this, WebUrls.LOGIN_API, loginInfoHashmap,
                        Request.Method.POST, responseActions, 1);
                apiRequest.sendRequest();
//                List<Object> x = CommonDialogs.showNotificationDialog(LoginActivity.this, "test", "OK");
            }
        });
    }

    @NonNull
    private VolleyResponseActions getVolleyResponseActions() {
        return new LoginResponse(this);
    }

    private Map getLoginApiMap() {
        Map mParams = new HashMap<>();
        mParams.put(JsonStringConstraints.LoginApiFields.KEY_USERNAME, username);
        mParams.put(JsonStringConstraints.LoginApiFields.KEY_PASSWORD, password);
        return mParams;
    }
}
