package com.example.payer.pmscs.commontask;

/**
 * Created by shuaibrahman on 10/17/16.
 */

public class CommonValues {
    public static final int LOCATION_PERMISSION_ID = 100;

    public static class SharedPrefKeyValuePairs {
        public static final String KEY_LOGGED_IN = "logged_in";
        public static final String VALUE_YES = "yes";
        public static final String VALUE_NO = "no";
    }
}
