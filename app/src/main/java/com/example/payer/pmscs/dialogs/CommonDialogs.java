package com.example.payer.pmscs.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.example.payer.pmscs.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shuaib on 2/3/16.
 */
public class CommonDialogs {

    public static List<Object> showChooseDialog(Context context, String title, String message, String yesButtonText, String noButtonText){
        Dialog alertDialog = new Dialog(context);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_layout);

        final String finalText = "<b>"+title+"</b><br/><br/>" + message;

        // set the custom progressDialog components - text, image and button
        TextView tvMessaage = (TextView) alertDialog.findViewById(R.id.title_tv);
        tvMessaage.setText(Html.fromHtml(finalText));
        TextView retry = (TextView) alertDialog.findViewById(R.id.retry_tv);
        retry.setText(yesButtonText);
        TextView cancel = (TextView) alertDialog.findViewById(R.id.cancel_tv);
        cancel.setText(noButtonText);

        alertDialog.show();

        List<Object> editTexts = new ArrayList<>();
        editTexts.add(retry);
        editTexts.add(cancel);
        editTexts.add(alertDialog);
        return editTexts;
    }


    public static List<Object> showNotificationDialog(Context context, String title, String okButonText){
        List<Object> chooseDialog = showChooseDialog(context, title, "", okButonText, "");
        ((TextView) chooseDialog.get(1)).setVisibility(View.GONE);
        ((Dialog)chooseDialog.get(2)).show();
        return chooseDialog;
    }

    public static ProgressDialog getApiCallProgressDialog(Context context){
        ProgressDialog progressDialog = new ProgressDialog(context, R.style.TransparentDialogTheme);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.setCancelable(false);
        return progressDialog;
    }
/*
    public static ProgressDialog getAssociationProgress(String message, String title, Context con){
        ProgressDialog mProgress = new ProgressDialog(con);
        mProgress.setTitle(title);
        mProgress.setMessage(message);
        if (title.equals(CommonTask.DEVICE_ADD_TEXT)) {
            mProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgress.setProgress(0);
            mProgress.setMax(100);
            mProgress.setIndeterminate(false);
        } else {
            mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgress.setIndeterminate(true);
        }

        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);
        mProgress.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MeshObjectInstances.mService.disconnectBridge();
            }
        });
        return mProgress;
    }*/
}
