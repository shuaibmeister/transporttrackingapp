package com.example.payer.pmscs.commontask;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.payer.pmscs.utils.CustomToast;

/**
 * Created by shuaib on 10/14/16.
 */

public class CommonTask {

    /*public static final HashMap<String, String> apiKeyValueMap = new HashMap<String, String>() {{
        put(JsonStringConstraints.CommonStrings.KEY_API, JsonStringConstraints.CommonStrings.VALUE_API);
    }};*/

    /*public static final List<HashMap<String, String>> apiKeyValueMapArray = new ArrayList<HashMap<String, String>>(){{
        add(CommonTask.apiKeyValueMap);
    }};*/

    public static void saveDataInPreference(Context context, String key, String value) {
        SharedPreferences.Editor editor = PreferenceManager
                .getDefaultSharedPreferences(context).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getPreferenceValue(Context context, String key){
        return PreferenceManager
                .getDefaultSharedPreferences(context).getString(key, "DEFAULT");
    }

    public static boolean isConnectedToInternet(Context context) {
        if (isNetworkAvailable(context)) {
            return true;
        } else {
            CustomToast.makeToast(context, "No internet connection. Please connect to internet and try again",
                    Toast.LENGTH_SHORT);
            return false;
        }
    }

    private static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        NetworkInfo.State networkState = null;
        if (connectivityManager != null) {
            networkInfo = connectivityManager.
                    getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (networkInfo != null) {
                networkState = networkInfo.getState();
                if (networkState == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
            networkInfo = connectivityManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (networkInfo != null) {
                networkState = networkInfo.getState();
                if (networkState == NetworkInfo.State.CONNECTED
                        ) {
                    return true;
                }
            }

        }
        return false;
    }

    public static LinearLayout.LayoutParams getLayoutParam(int left, int top, int right, int bottom){
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(left, top, right, bottom);
        return params;
    }
}
