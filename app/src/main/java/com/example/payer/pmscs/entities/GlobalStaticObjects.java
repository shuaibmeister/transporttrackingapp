package com.example.payer.pmscs.entities;

import com.example.payer.pmscs.entities.databaseobjects.RouteData;
import com.example.payer.pmscs.entities.databaseobjects.UserData;

import java.util.HashMap;

/**
 * Created by shuaib on 10/19/16.
 */

public class GlobalStaticObjects {
    public static UserData userData;
    public static HashMap<Long, RouteData> routeDatas = new HashMap<>();
    public static RouteData routeData;
}
