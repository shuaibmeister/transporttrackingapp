package com.example.payer.pmscs.utils.js;

/**
 * Created by shuaib on 10/18/16.
 */

public interface AssetReader {
    String readFile(String fileName);
}
