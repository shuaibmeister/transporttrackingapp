package com.example.payer.pmscs.utils;

import android.util.Log;

/**
 * Created by Fahim Hossain (fahim9n) on 9/1/2016.
 */
public class CustomLog {

    public static boolean isDebug = true;

    public static void logD(String tag, String msg) {
        if (isDebug) {
            Log.d(tag, msg);
        }
    }

    public static void logE(String tag, String msg) {
        if (isDebug) {
            Log.e(tag, msg);
        }
    }

    public static void logE(String tag, String msg, Throwable tr) {
        if (isDebug) {
            Log.e(tag, msg, tr);
        }
    }

    public static void print(String msg) {
        if (isDebug) {
            System.out.println(msg);
        }
    }
}
