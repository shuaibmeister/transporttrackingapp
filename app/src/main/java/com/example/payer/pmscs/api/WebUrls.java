package com.example.payer.pmscs.api;

/**
 * Created by shuaib on 4/7/16.
 */
public final class WebUrls {
    private static final String BASE_API_URL = "http://120.50.8.56/api/";

    public static final String LOGIN_API = BASE_API_URL + "auth/login/";
    public static final String REGISTRATION_API = BASE_API_URL + "auth/create_user/";
    public static final String ROUTE_FETCH_API = BASE_API_URL + "map/getAllRoutes/";
    public static final String TRACKING_API = BASE_API_URL + "location/updateTrackingLocation/";
    public static final String WAITING_API = BASE_API_URL + "location/updateMyLocation/";

    public static String getBusPositionApi(long routeId){
        return BASE_API_URL + "map/trackRouteByCurrentPosition/"+routeId;
    }
}
