package com.example.payer.pmscs.utils.location;

import com.example.payer.pmscs.entities.Observer;
import com.example.payer.pmscs.entities.databaseobjects.TrackingData;

/**
 * Created by shuaibrahman on 10/17/16.
 */

public interface PositionManager {
    TrackingData getLocation();
    long getMinTime();
    float getMinDistance();
    void setMinTime(long minTime);
    void setMinDistance(float minDistance);
    void setNotifyObserver(Observer notifyObserver);
    Observer getNotifyObserver();
    void startTracking();
    void stopTracking();
}
