package com.example.payer.pmscs.volley.ApiResponseActions;

import com.android.volley.VolleyError;
import com.example.payer.pmscs.utils.CustomLog;
import com.example.payer.pmscs.volley.VolleyResponseActions;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by shuaib on 10/19/16.
 */

public class LocationApiCallResponse implements VolleyResponseActions {

    @Override
    public void onApiCallSuccess(int reqId, JSONObject responseJson) throws JSONException {
        CustomLog.print("check in data sent to server");
    }

    @Override
    public void onApiCallFailure(int reqId, JSONObject responseJson) throws JSONException {
        CustomLog.print("check in data could not be sent to server");
    }

    @Override
    public void onApiCallError(int reqId, VolleyError error) {
    }
}
