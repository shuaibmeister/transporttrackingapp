package com.example.payer.pmscs.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by shuaib on 10/19/16.
 */

public class CustomToast extends Toast {

    public CustomToast(Context context) {
        super(context);
    }

    public static void makeToast(Context context, String message, int duration){
        Toast.makeText(context, message, duration).show();
    }
}
