package com.example.payer.pmscs.utils.location;

import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.payer.pmscs.utils.js.AssetReader;


/**
 * Created by shuaib on 10/18/16.
 */

public class WebClient extends WebViewClient {

    private AssetReader jsAssetReader;

    public WebClient(AssetReader jsLoader){
        super();
        this.jsAssetReader = jsLoader;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        view.loadUrl("javascript:"+ jsAssetReader.readFile("loadMyLocation.js"));
//        view.loadUrl("javascript:"+ jsAssetReader.readFile("test.js"));
    }
}
