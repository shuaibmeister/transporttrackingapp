package com.example.payer.pmscs.volley;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by shuaib on 4/10/16.
 */
public interface VolleyResponseActions {
    void onApiCallSuccess(int reqId, JSONObject responseJson) throws JSONException;
    void onApiCallFailure(int reqId, JSONObject responseJson) throws JSONException;
    void onApiCallError(int reqId, VolleyError error);
}
