package com.example.payer.pmscs.utils.js;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.Scriptable;

import java.util.HashMap;

/**
 * Created by shuaib on 10/18/16.
 */

public class JavaToJs {

    private String jsFileContent;
    private HashMap<String, Object[]> argumentList;

    public JavaToJs(String jsFile, HashMap<String, Object[]> functikonArguments) {
        this.jsFileContent = jsFile;
        this.argumentList = functikonArguments;
    }

    public Object callFunction(String functionName) {
        String result = "";
        Object[] params = argumentList.get(functionName);

        Context rhino = Context.enter();

        rhino.setOptimizationLevel(-1);
        try {
            Scriptable scope = rhino.initStandardObjects();

            rhino.evaluateString(scope, jsFileContent, "JavaScript", 1, null);

            Object obj = scope.get(functionName, scope);

            if (obj instanceof Function) {
                Function jsFunction = (Function) obj;

                Object jsResult = jsFunction.call(rhino, scope, scope, params);
                result = Context.toString(jsResult);
            }
        } finally {
            Context.exit();
        }
        return result;
    }
}
