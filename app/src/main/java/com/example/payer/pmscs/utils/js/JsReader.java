package com.example.payer.pmscs.utils.js;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by shuaib on 10/18/16.
 */

public class JsReader implements AssetReader {

    private Context context;
    private final String jsLocation = "www/js/";

    public JsReader(Context context) {
        this.context = context;
    }

    @Override
    public String readFile(String fileName) {
        String tContents = "";

        try {
            InputStream stream = context.getAssets().open(jsLocation + fileName);

            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            tContents = new String(buffer);
        } catch (IOException e) {
            e.printStackTrace();
            // Handle exceptions here
        }

        return tContents;
    }
}
