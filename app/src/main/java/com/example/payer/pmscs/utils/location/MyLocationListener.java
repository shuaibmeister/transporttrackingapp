package com.example.payer.pmscs.utils.location;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.example.payer.pmscs.entities.GlobalStaticObjects;
import com.example.payer.pmscs.entities.Observer;
import com.example.payer.pmscs.entities.databaseobjects.TrackingData;
import com.example.payer.pmscs.utils.CustomLog;

/**
 * Created by shuaibrahman on 10/17/16.
 */
public class MyLocationListener implements LocationListener {

    private Observer observer;
    private TrackingData locationData;

    @Override
    public void onLocationChanged(Location location) {
        CustomLog.print(location.toString());

        locationData = getTrackingObject(location);
        observer.onUpdate(locationData);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public Observer getObserver() {
        return observer;
    }

    public void setObserver(Observer observer) {
        this.observer = observer;
    }

    private TrackingData getTrackingObject(Location location) {
        TrackingData locationData = new TrackingData();
        locationData.setLat(location.getLatitude());
        locationData.setLon(location.getLongitude());
        locationData.setAccuracy(location.getAccuracy());
        locationData.setUserId(GlobalStaticObjects.userData.getId());
        return locationData;
    }
}
