package com.example.payer.pmscs.activities.registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.example.payer.pmscs.R;
import com.example.payer.pmscs.activities.map.MapViewActivity;
import com.example.payer.pmscs.api.JsonStringConstraints;
import com.example.payer.pmscs.api.WebUrls;
import com.example.payer.pmscs.activities.BaseActivity;
import com.example.payer.pmscs.commontask.CommonTask;
import com.example.payer.pmscs.commontask.CommonValues;
import com.example.payer.pmscs.entities.ApiDataObject;
import com.example.payer.pmscs.entities.ApiDataObjectImpl;
import com.example.payer.pmscs.activities.login.LoginActivity;
import com.example.payer.pmscs.entities.GlobalStaticObjects;
import com.example.payer.pmscs.entities.databaseobjects.UserData;
import com.example.payer.pmscs.utils.CustomToast;
import com.example.payer.pmscs.volley.ApiResponseActions.LoginResponse;
import com.example.payer.pmscs.volley.SendVolleyApiRequest;
import com.example.payer.pmscs.volley.VolleyApiRequest;
import com.example.payer.pmscs.volley.VolleyResponseActions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    private String firstName, lastName, userID, password, email, phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final EditText etFirstName = (EditText) findViewById(R.id.firstName);
        final EditText etLastName = (EditText) findViewById(R.id.lastName);
        final EditText etUserID = (EditText) findViewById(R.id.userID);
        final EditText etPassword = (EditText) findViewById(R.id.password);
        final EditText etEmail = (EditText) findViewById(R.id.email);
        final EditText etPhone = (EditText) findViewById(R.id.phone);
        final Button btnSignUp = (Button) findViewById(R.id.btnSignUp);
        final Button btnBack = (Button) findViewById(R.id.btnBack);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstName = etFirstName.getText().toString();
                lastName = etLastName.getText().toString();
                userID = etUserID.getText().toString();
                password = etPassword.getText().toString();
                email = etEmail.getText().toString();
                phone = etPhone.getText().toString();

//                Map params = getRegistrationHashmap();
                VolleyResponseActions responseActions = getVolleyResponseActions();
                ApiDataObject params = new ApiDataObjectImpl(getRegistrationHashmap());
                VolleyApiRequest apiRequest = new SendVolleyApiRequest(RegisterActivity.this, WebUrls.REGISTRATION_API,
                        params, Request.Method.POST, responseActions, 1);
                apiRequest.sendRequest();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(RegisterActivity.this, LoginActivity.class);
                RegisterActivity.this.startActivity(loginIntent);
            }
        });
    }

    @NonNull
    private VolleyResponseActions getVolleyResponseActions() {
        return new LoginResponse(this);
    }

    private Map getRegistrationHashmap() {
        Map params = new HashMap<>();
        params.put("first_name", firstName);
        params.put("last_name", lastName);
        params.put("username", userID);
        params.put("password", password);
        params.put("email", email);
        params.put("phone", phone);
        return params;
    }
}
